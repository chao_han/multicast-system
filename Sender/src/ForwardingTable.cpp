/*
 * ForwardTable.cpp
 *
 *  Created on: 2013-5-2
 *      Author: lee
 */

#include "ForwardingTable.h"

using namespace std;

ForwardingTable::ForwardingTable(unsigned char src, std::vector<unsigned char> *dsts,vector<vector<pair<unsigned char,unsigned char> > > routerGraph) {
	// TODO Auto-generated constructor stub
	
	g=new RouterGraph();
	fdp=new FwdTableData(0,0,0,0);
	this->src=src;
	this->dsts=dsts;
	this->setFwdTable(routerGraph);
	

}




vector<unsigned char> ForwardingTable::getNextHop(){
	return nextHop; 
}

vector<int> ForwardingTable::getCost(){
	return cost;
}

int ForwardingTable::getSize(){
	return dsts->size();
}

void ForwardingTable::printTable(){
	for(int i=0;i<table.size();i++){
		*fdp=table[i];   
		cout<<int(fdp->getDestAddr())<<"     "<<fdp->getCost()<<"     "<<int(fdp->getPortNumAndNextHop().first)<<"       "<<int(fdp->getPortNumAndNextHop().second)<<endl;
	}
}


void ForwardingTable::insert(FwdTableData *fdata){
	table.push_back(*fdata);
}


void ForwardingTable::setFwdTable(vector<vector<pair<unsigned char,unsigned char> > > routerGraph){

	cost.resize(dsts->size());
	cout<<"we have "<<dsts->size()<<"destinations"<<endl;
	nextHop.resize(dsts->size());
	for(unsigned int i=0;i<dsts->size();i++){
		g->dijkstra(routerGraph,int(src),int(dsts->at(i)),path);
		g->setPath(path);
		cost.at(i)=g->getCost();
		nextHop.at(i)=g->getNextHop();
		fwdData=new FwdTableData(dsts->at(i),cost.at(i),0,nextHop.at(i));
		this->insert(fwdData);	
		
	}
	
	this->printTable();
	
}



ForwardingTable::~ForwardingTable() {
	// TODO Auto-generated destructor stub
}

