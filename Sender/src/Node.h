/*
 * Node.h
 *
 *  Created on: May 1, 2013
 *      Author: chaohan
 */

#ifndef NODE_H_
#define NODE_H_

#include<string>
#include"Util.h"
#include"LinkLayer.h"
#include"HelloPktTimer.h"
#include<queue>
const std::string configFile = "config.ini";

//class NodeTimer;

class PacketFormat;
class HelloPktTimer;
class Node{
public:
	Node();
	Node(int cnt);

	int getPortCnt();
	void setPortCnt(int cnt);

	static unsigned char getNodeAddr();
	//static
	void setNodeAddr(unsigned char addr);

	//void switchSendingSeqNum();

	//virtual void send() = 0;
	virtual void receive () = 0;
	virtual void initialize() = 0;
	virtual void process() = 0;
	virtual void helloPktTimerHandler() = 0;
	virtual void lspPktTimerHandler() = 0;
	//virtual void* createPortThread(void*) = 0;
	static void pushBuf(PacketFormat* );
	static PacketFormat* popBuf();
	virtual ~Node();
protected:
	virtual void readConfigFile() = 0 ;
	int portCnt;
	HelloPktTimer *helloTimer;

	static unsigned char nodeAddr;
	static std::string *configFilename;
	static std::queue<PacketFormat*> *routerBuffer;
};
//std::string Node::filename = "config.ini";

#endif /* NODE_H_ */
