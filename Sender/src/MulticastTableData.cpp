/*
 * MulticastTable.cpp
 *
 *  Created on: May 5, 2013
 *      Author: loki
 */

#include "MulticastTableData.h"
#include <iostream>

using namespace std;

MulticastTableData::MulticastTableData() {
	// TODO Auto-generated constructor stub
	nextHop = new std::vector<unsigned char>();
	cost = 0;
}

MulticastTableData::MulticastTableData(std::vector<unsigned char> *nextHopForEachDest, std::vector<int> *nCost) {
	// TODO Auto-generated constructor stub
	nextHop = nextHopForEachDest ;
	int count = 1;
	if(nextHop->size()==2){
		if(nextHop[0]!=nextHop[1]) count++;
	}
	else if(nextHop->size()==3){
		if(nextHop->at(0)!=nextHop->at(1)){
			count++;
			if(nextHop->at(2)!=nextHop->at(2)&&nextHop->at(2)!=nextHop->at(1)) {count++;}
		}
		else if(nextHop->at(2)!=nextHop->at(0)) {count++;}
	}
	cout<<"          "<<count<<endl;
	int sumOfNCost=0;
	unsigned int i;
	for(i=0;i<nCost->size();i++) sumOfNCost+=nCost->at(i);
	cost=sumOfNCost;
	cost+=count*int(nCost->size());
}

double MulticastTableData::getCost(){
	return cost;
}

std::vector<unsigned char> *MulticastTableData::getnextHop(){
	return nextHop;
}

void MulticastTableData::print(){
	unsigned int i;
	std::cout<<"Destination: ";
	for(i=0;i<nextHop->size();i++)
		std::cout<<nextHop->at(i)<<" ";
	std::cout<<"Multicast cost: "<<cost<<std::endl;
}

MulticastTableData::~MulticastTableData() {
	// TODO Auto-generated destructor stub
}


