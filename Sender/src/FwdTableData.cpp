/*
 * ForwardPair.cpp
 *
 *  Created on: 2013-5-2
 *      Author: lee
 */

#include "FwdTableData.h"

FwdTableData::FwdTableData(unsigned char addr,int cost,int portNum,
		unsigned char nextHop):destAddr(addr),cost(cost){
	// TODO Auto-generated constructor stub
	portNumAndNextHop=std::make_pair(portNum,nextHop);
}


unsigned char FwdTableData::getDestAddr(){
	return destAddr;
}

int FwdTableData::getCost(){
	return cost;
}

std::pair<unsigned char,unsigned char> FwdTableData::getPortNumAndNextHop(){
	return portNumAndNextHop;
}

void FwdTableData::setPortNumAndNextHop(int portNum, unsigned char nextHop){
	portNumAndNextHop = std::make_pair(portNum, nextHop);
}

FwdTableData::~FwdTableData() {
	// TODO Auto-generated destructor stub

}

