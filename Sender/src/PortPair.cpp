#include "PortPair.h"

//constructor
PortPair::PortPair(){
	conf = new PortConfig();
	txPort = new mySendingPort();
	txPort->setACKflag(true);
	rxPort = new LossyReceivingPort(pktLossProbability);
	linkLayer = new LinkLayer();
	isHelloAcked = 1;
	portMutex = PTHREAD_MUTEX_INITIALIZER;
}
PortPair::PortPair(PortConfig *config){
	conf = config;
	txPort = new mySendingPort();
	txPort->setACKflag(true);
	rxPort = new LossyReceivingPort(pktLossProbability);
	linkLayer = new LinkLayer();
	isHelloAcked = 1;
}

PortPair::~PortPair(){
	delete [] txPort;
	delete [] rxPort;
	delete [] linkLayer;
}

void PortPair::setPortConfig(PortConfig *config){
	conf = config;
}

PortConfig* PortPair::getPortConfig(){
	return conf;
}

char PortPair::getLinkLayerSeq(){
	return linkLayer->getSeqNum();
}

void PortPair::setLinkLayerSeq(char seq){
	linkLayer->setSeqNum(seq);
}

void PortPair::switchLinkLayerSeq(){
	pthread_mutex_lock(&portMutex);
	char currentSeq = linkLayer->getSeqNum();
	if(currentSeq == 0){
		linkLayer->setSeqNum(1);
	} else {
		linkLayer->setSeqNum(0);
	}
	pthread_mutex_lock(&portMutex);
}

//port initialization
void PortPair::initialize(){
	const char* hostname = "localhost";
	Address *txAddr = new Address(hostname, conf->getSendingPortNo());
	Address *nextHopAddr = new Address(hostname, conf->getNextHopPortNo());
	Address *rxAddr = new Address(hostname, conf->getReceivingPortNo());

	//initialize sending port, next hop port
	txPort->setAddress(txAddr);
	txPort->setRemoteAddress(nextHopAddr);
	txPort->init();

	//initialize receiving port.
	rxPort->setAddress(rxAddr);
	rxPort->init();
}

void PortPair::send(Packet *pkt){
	//pthread_mutex_lock(&portMutex);
	//encapsulate the packet in the link layer
	pkt = linkLayer->encapsulate(pkt);
	txPort->sendPacket(pkt);
	txPort->timer_.startTimer(5);
	txPort->setACKflag(false);        // other message cannot be sent from this port;
	txPort->lastPkt_ = pkt;
	/*set timeout*/
	std::cout<<"start timer"<<std::endl;

	std::cout << "link layer packet "
			<<(int)pkt->accessHeader()->getOctet(0)<<
			" begin waiting for ACK..." <<std::endl;
	//pthread_mutex_unlock(&portMutex);
}

void PortPair::sendLinkLayerAck(Packet *pkt){
	pkt = linkLayer->encapsulate(pkt);
	txPort->sendPacket(pkt);
	std::cout << "Sending link layer Ack packet "
			<<(int)pkt->accessHeader()->getOctet(0)<<std::endl;
}

void PortPair::sendHelloAck(){
	std::cout<<"send Hello Ack packet"<<std::endl;
	PacketFormat *pktfmt = new PacketHelloAck(conf->getAddr());
	send(pktfmt->getPacket());
}

void PortPair::sendHello(){
	std::cout<<"send Hello Packet"<<std::endl;
	PacketFormat *pktfmt = new PacketHello(conf->getAddr());
	send(pktfmt->getPacket());
}

void PortPair::receive(){
	Packet* linkLayerPkt;
	Packet* pkt;
	Packet* pktAck = new Packet();

	int linkLayerPktseqNum = 0 ;
	int receivedSeqNum = -1;
	int linkLayerPktPayload;
	while(1){
		//listen to the receiving port
		linkLayerPkt = rxPort->receivePacket();

		//if a packet arrive
		if(linkLayerPkt != NULL){
			PacketFormat *pktfmt;
			//get sequence no of the link layer packet 0000 0000 or 0000 0001
			linkLayerPktseqNum = linkLayerPkt->accessHeader()->getOctet(0);
			linkLayerPktPayload = linkLayerPkt->getPayloadSize();

			/*if payload of link layer packet is not empty(0), the packet is not an Ack packet,
			 * and an Ack encapsulated packet is necessary.
			 */
			if((linkLayerPktPayload != 1) ){
				//remember the current link Layer seq Num
				char currentSeqNum = linkLayer->getSeqNum();

				//set seqNum to link layer
				linkLayer->setSeqNum(linkLayerPktseqNum);

				std::cout<<"sending Link Layer ACK with sequence Num "<<linkLayerPktseqNum<<std::endl;
				sendLinkLayerAck(pktAck);

				//set current back after sending ACK;
				linkLayer->setSeqNum(currentSeqNum);

			} else if((linkLayerPktPayload == 1 )&&     //if it is an Ack;
					(linkLayerPktseqNum == linkLayer->getSeqNum())){ //seqNum in linklayer Packet equals packect just sent
				linkLayer->switchSeqNum();
				txPort->setACKflag(true);               //set ACK flag as true, which means it can send other packet
				txPort->timer_.stopTimer();             //stops waiting for timeout;
				std::cout<<"message with sequence Num:" <<linkLayerPktseqNum <<"has received."<<std::endl;
				std::cout<<"---------------------------------------------------------------"<<std::endl;

				continue;
			} else{
				//std::cout<<"Packet with sequence Num:"<<linkLayerPktseqNum<<"is wrong SeqNum, drop packet"<<std::endl;
				std::cout<<"error"<<std::endl;
				continue;
			}


			if(receivedSeqNum == linkLayerPktseqNum){
				std::cout<<"packet has received before"<<std::endl;
				continue;
			}

			receivedSeqNum = linkLayerPktseqNum;

			pkt = linkLayer->extract(linkLayerPkt);
			char packetType = pkt->accessHeader()->getOctet(0);
			//find out the type of received packet;
			switch(packetType){
			//Hello 0000 0000;
			case 0:
				pktfmt = new PacketHello(pkt);
				std::cout<<"receive a hello Packet"<<std::endl;
				conf->setNextHopAddr(pktfmt->getSrcAddr());
				conf->printPortConfig();
				//create a hello ack, and push to buffer;
				pktfmt = new PacketHelloAck(conf->getAddr());
				//Hello Ack has not received yet, set isHelloAcked Flag as 0;
				isHelloAcked = 0;
				Router::pushBuf(pktfmt);
				std::cout<<"a Hello ACK packet pushed into buffer"<<std::endl;
				break;
			//Hello Ack 0000 0001;
			case 1:
				pktfmt = new PacketHelloAck(pkt);
				std::cout<<"receive a hello ACK"<<std::endl;
				conf->setNextHopAddr(pktfmt->getSrcAddr());
				//Hello just send is acked;

				break;
			//Link State 0000 0010;
			case 2:
				std::cout<<"sender receive a lsp, drop"<<std::endl;
				/*pktfmt = new PacketLinkState(pkt);
				std::cout<<"receive a link state packet"<<std::endl;
				if( pktfmt->processPacket() != 0 ) {
					Router::printLinkStateTable();
					Node::pushBuf(pktfmt);
				} else {
					std::cout<<"drop LSP"<<std::endl;
				}*/
				break;
			//Data Packet 0000 0011;
			case 3:
				std::cout<<"sender receive a data packet,  drop"<<std::endl;
				/*pktfmt = new PacketData(pkt);
				std::cout<<"receive a data packet"<<std::endl;
				if(pktfmt->processPacket() != 0) {
					Node::pushBuf(pktfmt);
				} else {
					std::cout<<"destination or drop"<<std::endl;
					pktfmt->print();
				}*/
				break;
			default:
				std::cout<<"error: no such packet type, check you code"<<std::endl;
				break;
			}
		}
	}
}

void PortPair::printConfigInfo(){
	conf->printPortConfig();
}
