#ifndef PACKETFORMAT_H
#define PACKETFORMAT_H
#include "common.h"
#include"Sender.h"
#include"Node.h"
#include<iostream>
#include<vector>
#include<string>

class Node;

class PacketFormat
{
    public:
        PacketFormat();
        PacketFormat(Packet *pkt);

        unsigned char getPktType();

        virtual Packet* getPacket();
        virtual void setPacket(Packet *pkt);

        virtual unsigned char getSrcAddr() = 0;
        virtual void print() = 0;
        virtual bool processPacket()=0;

        virtual ~PacketFormat();
    protected:
        Packet* my_packet;
};

class PacketHello: public PacketFormat {
public:
	PacketHello(unsigned char SrcAddr);
	PacketHello(Packet *pkt);
	unsigned char getSrcAddr();
	bool processPacket();
	void print();

protected:
private:
};

class PacketHelloAck: public PacketFormat {
public:
	PacketHelloAck(unsigned char SrcAddr);
	PacketHelloAck(Packet *pkt);
	unsigned char getSrcAddr();
	bool processPacket();
	void print();
protected:
private:
};

class PacketLinkState: public PacketFormat {
public:
	PacketLinkState(unsigned char SeqNum, unsigned char SrcAddr,
			std::vector<unsigned char> *destAddrs);
	PacketLinkState(Packet *pkt);
	unsigned char getSrcAddr();
	bool processPacket();
	void print();
	char getSeqNum();
	char getNeighborCnt();
	char getNeighborAddr(int index);
protected:
private:
};

class PacketData: public PacketFormat {
public:
	PacketData(unsigned char TTL, unsigned char srcAddr,
			std::vector<unsigned char> *destAddrs, const std::string &msg);
	PacketData(Packet *pkt);

	unsigned char getSrcAddr();
	bool processPacket();
	void print();
	char getTTL();
	char getNumOfAddrs();
	int getDataSize();
	char getDestAddr(int index);
	void setTTL(char ttl);
	void setDesAddrs(std::vector<unsigned char> *desAddrs);
protected:
private:
	//unsigned getNodeAddr(Node *node);
};


#endif // PACKETFORMAT_H

