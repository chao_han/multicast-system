/*
 * PortThreadData.h
 *
 *  Created on: May 5, 2013
 *      Author: chaohan
 */

#ifndef PORTTHREADDATA_H_
#define PORTTHREADDATA_H_

#include"PortPair.h"

class PortPair;
class PortThreadData{
public:
	PortThreadData(int id, PortPair* port){
		threadId = id;
		portPair = port;
	}
	PortThreadData(){
		threadId = 0;
		portPair = 0;
	}
	int threadId;
	PortPair* portPair;
};


#endif /* PORTTHREADDATA_H_ */
