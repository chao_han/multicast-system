/*
 * Graph.h
 *
 *  Created on: 2013-5-2
 *      Author: lee
 */

#ifndef ROUTERGRAPH_H_
#define ROUTERGRAPH_H_
#include<vector>
#include<utility>
using std::vector;
using std::pair;

class RouterGraph {
public:
	RouterGraph();
	int getSize();
	int getCost();

	unsigned char getNextHop();
	vector<unsigned char> getPath();
	vector<vector<pair<unsigned char,unsigned char> > > getGraph();
	void printGraph();
	void setSize(int);

	void update(unsigned char, vector<unsigned char>);
	static void dijkstra(vector<vector<pair<unsigned char,unsigned char> > > &G,
			const unsigned char &source,const unsigned char &destination,vector<unsigned char> &path);
	void setPath(std::vector<unsigned char>);
	//int checkConnected(unsigned char,unsigned char);
	virtual ~RouterGraph();

private:
	void insert(unsigned char,unsigned char);
	void remove(unsigned char);


	vector<vector<pair<unsigned char,unsigned char> > > graph;
	vector<unsigned char> path;
	vector<unsigned char> dsts;
	//vector<unsigned char> checkSet;
	unsigned int index;
	int oriSize;
};

#endif /* GRAPH_H_ */

