#include "common.h"
#include <iostream>
using namespace std;

class mySendingPort :public SendingPort
{

public:
	mySendingPort(){
		myMutex = PTHREAD_MUTEX_INITIALIZER;
	}
  inline void setACKflag(bool flag){ ackflag_ =flag;}
  inline bool isACKed(){return ackflag_;}
  inline void timerHandler()
  {
    if (!ackflag_)
      {       
       cout << "The last sent packet has not been acknowledged yet. Re-send..." <<endl;
       //cout<< (int)lastPkt_->accessHeader()->getOctet(0)<<std::endl;
       sendPacket(lastPkt_);       
       //schedule a timer again
       timer_.startTimer(5);
      }              
  }  
private:
  bool ackflag_;
  pthread_mutex_t myMutex;
public:
  Packet * lastPkt_;
};
