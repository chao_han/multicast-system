/*
 * ForwardTable.h
 *
 *  Created on: 2013-5-2
 *      Author: lee
 */
#ifndef FORWARDINGTABLE_H_
#define FORWARDINGTABLE_H_

#include "FwdTableData.h"
#include "RouterGraph.h"
#include <vector>
#include<iostream>
#include<vector>


class ForwardingTable {
public:
	ForwardingTable(unsigned char src, std::vector<unsigned char> *dsts,vector<vector<pair<unsigned char,unsigned char> > > routerGraph);
	void insert(FwdTableData *);
	void setFwdTable(vector<vector<pair<unsigned char,unsigned char> > > routerGraph);
	void printTable();
	int getSize();
	vector <unsigned char> getNextHop();
	vector <int> getCost();
	virtual ~ForwardingTable();
private:
	unsigned char src;
	int destAddr;
	FwdTableData *fdp;
	std::vector<FwdTableData> table;
	FwdTableData *fwdData;
	RouterGraph *g;
	vector<vector<pair<unsigned char,unsigned char> > > routerGraph;
	vector<unsigned char> path;
	vector<unsigned char> *dsts;	
	vector<int> cost;
	vector<unsigned char> nextHop;
	std::pair<int,int> portNumAndNextHop;
};

#endif /* FORWARDTABLE_H_ */
