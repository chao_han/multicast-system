/*
 * Graph.cpp
 *
 *  Created on: 2013-5-2
 *      Author: lee
 */

#include "RouterGraph.h"
#include "Util.h"
#include "ForwardingTable.h"
#include<iostream>
#include <algorithm>
#include <limits>
#include <queue>
using std::make_pair;
using namespace std;


RouterGraph::RouterGraph() {
	// TODO Auto-generated constructor stub
	//graph = new vector<vector<pair<unsigned char,unsigned char> > >();
	oriSize=0;
}

	
vector<vector<pair<unsigned char,unsigned char> > > RouterGraph::getGraph(){
	return graph;
}

void RouterGraph::setPath(vector<unsigned char> p){
	path=p;
}

vector<unsigned char> RouterGraph::getPath(){
	return path;
}


void RouterGraph::dijkstra(vector<vector<pair<unsigned char,unsigned char> > > &G,
		const unsigned char &source,const unsigned char &destination,vector<unsigned char> &path){
	//if(checkConnected(source,destination)){
	vector<float> d(G.size());
	vector<int> parent(G.size());
	for(unsigned int i = 0 ;i < G.size(); i++){
		d[i] = std::numeric_limits<float>::max();
		parent[i] = -1;
	}
	priority_queue<pair<int,float>, vector<pair<int,float> >, Comparator> Q;
	d[source] = 0.0f;
	Q.push(make_pair(source,d[source]));
	while(!Q.empty()){
		int u = Q.top().first;
		if(u==destination) break;
		Q.pop();
		for(unsigned int i=0; i < G[u].size(); i++){
			int v= G[u][i].first;
			float w = G[u][i].second;
			if(d[v] > d[u]+w){
				d[v] = d[u]+w;
				parent[v] = u;
				Q.push(make_pair(v,d[v]));
			}
		}
	}
	path.clear();
	int p = destination;
	path.push_back(destination);
	while(p!=source){
		p = parent[p];
		path.push_back(p);
	}
}


unsigned char RouterGraph::getNextHop(){
	int size=path.size();
	if(size>2) return path[path.size()-2];
	if(size==2) return path[1];
	if(size==1) return path[0];
}

int RouterGraph::getCost(){
	return path.size()-1;
}

void RouterGraph::printGraph(){
	for(unsigned int i=0;i<graph.size();i++){
		for(unsigned int j=0;j<graph[i].size();j++){
			std::cout<<i<<" --> "<<int(graph[i][j].first)<<endl;
		}
	}
}

/////////////////////update Graph info.

void RouterGraph::update(unsigned char src, vector<unsigned char> dst){
	remove(src);
	for(index=0;index<dst.size();index++){
		insert(src,dst.at(index));
	}
}


void RouterGraph::insert(unsigned char srcAddr,unsigned char dstAddr){
	graph[srcAddr].push_back(make_pair(dstAddr,1));
	graph[dstAddr].push_back(make_pair(srcAddr,1));

}

void RouterGraph::remove(unsigned char srcAddr){
	unsigned int j;
	//cout<<"the removed size is "<<graph[srcAddr].size()<<endl;
	unsigned int size=graph[srcAddr].size();
	for(index=0;index<size;index++){
		//cout<<"the back up dst in"<<index<<" is "<<graph[srcAddr][0].first<<endl;		
		dsts.push_back(graph[srcAddr][0].first);
		graph[srcAddr].erase(graph[srcAddr].begin());
	}
	
	for(index=0;index<dsts.size();index++){
		//cout<<"the related removed size is "<<dsts.size()<<endl;
		int dstName=dsts.at(index);
		//cout<<"the removed addr is "<<dstName<<endl;
		for(j=0;j<graph[dstName].size();j++){
			if(graph[dstName][j].first==srcAddr){
				graph[dstName].erase(graph[dstName].begin()+j);
			}
		
		}
	}
}

int RouterGraph::getSize(){
	return graph.size();
}

void RouterGraph::setSize(int size){
	if(oriSize>=size) return;
	else graph.resize(size);
}

RouterGraph::~RouterGraph() {
	// TODO Auto-generated destructor stub
}



