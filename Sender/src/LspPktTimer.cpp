/*
 * LspPktTimer.cpp
 *
 *  Created on: May 7, 2013
 *      Author: chaohan
 */


#include"LspPktTimer.h"

LspPktTimer::LspPktTimer(Router *router){
	this->router = router;
	tdelay_.tv_nsec = 0;
	tdelay_.tv_sec =  0;
}



void LspPktTimer::startTimer (double delay) {
	tdelay_.tv_nsec = (long int)((delay - (int)delay)*1e9);
	tdelay_.tv_sec =  (int)delay;
	int error = 0;
		error = pthread_create(&tid, NULL, &timerProc, this );
	if (error)
		throw "Timer thread creation failed...";
}

void LspPktTimer::stopTimer(){
	  pthread_cancel(tid);
}


void *LspPktTimer::timerProc(void *arg){
	LspPktTimer *timer = (LspPktTimer *)arg;
	nanosleep(&(timer->tdelay_), NULL);
	timer->router->lspPktTimerHandler();
	return NULL;
}
