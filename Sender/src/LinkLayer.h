/*
 * LinkLayer.h
 *
 *  Created on: May 2, 2013
 *      Author: chaohan
 */

#ifndef LINKLAYER_H_
#define LINKLAYER_H_

#include"common.h"
#include<iostream>
#include<string>

//const int linkLayerBufSize = 1499;
const int linkLayerMaxPayloadSize = 1499;

class LinkLayer{
public:
	LinkLayer();
	//Packet* getPacket();
	Packet* encapsulate(Packet* pkt);
	Packet* extract(Packet* pkt);
	char getSeqNum();
	void setSeqNum(char num);
	void switchSeqNum();
	~LinkLayer();
private:
	//Packet* myPacket;
	char seqNum;
	//std::string *buffer;
};


#endif /* LINKLAYER_H_ */
