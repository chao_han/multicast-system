/*
 * Config.cpp
 *
 *  Created on: May 1, 2013
 *      Author: chaohan
 */

#include"PortConfig.h"

PortConfig::PortConfig(){
	sendingPortNo = invalidPortNum;
	receivingPortNo = invalidPortNum;
	nextHopPortNo = invalidPortNum;
	addr = invalidAddr;
	nextHopAddr = invalidAddr;
}

PortConfig::PortConfig(int Addr,int sPort, int rPort, int nxtPort):
		addr(Addr),sendingPortNo(sPort), receivingPortNo(rPort),
		nextHopPortNo(nxtPort), nextHopAddr(invalidAddr){
}

PortConfig::PortConfig(int Addr,int sPort, int rPort, int nxtPort,
		 int nxtAddr): addr(Addr),sendingPortNo(sPort),
				receivingPortNo(rPort), nextHopPortNo(nxtPort),
				nextHopAddr(nxtAddr){
}

void PortConfig::printPortConfig(){
	std::cout<<addr<<" "<<sendingPortNo<<" "<<receivingPortNo<<" "
			<<nextHopPortNo<<" "<<nextHopAddr<<std::endl;
}

void PortConfig::setSendingPortNo(int sPort){
	sendingPortNo = sPort;
}

void PortConfig::setReceivingPortNo(int rPort){
	receivingPortNo = rPort;
}

void PortConfig::setNextHopPortNo(int nPort){
	nextHopPortNo = nPort;
}

void PortConfig::setNextHopAddr(int nxtAddr){
	nextHopAddr = nxtAddr;
}

void PortConfig::setAddr(int Addr){
	addr = Addr;
}

int PortConfig::getSendingPortNo(){
	return sendingPortNo;
}
int PortConfig::getReceivingPortNo(){
	return receivingPortNo;
}
int PortConfig::getNextHopPortNo(){
	return nextHopPortNo;
}
int PortConfig::getNextHopAddr(){
	return nextHopAddr;
}
int PortConfig::getAddr(){
	return addr;
}
