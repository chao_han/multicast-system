/*
 * LspPktTimer.h
 *
 *  Created on: May 7, 2013
 *      Author: chaohan
 */

#ifndef LSPPKTTIMER_H_
#define LSPPKTTIMER_H_

#include"Sender.h"
#include<pthread.h>

class Router;

class LspPktTimer {
public:
	LspPktTimer(Router *router);
	void startTimer(double delay);
	void stopTimer();
	//create a separate thread;
	static void *timerProc(void *arg) ;
	~LspPktTimer();
protected:
	Router *router;
	struct timespec tdelay_;
	pthread_t tid;
};


#endif /* LSPPKTTIMER_H_ */
