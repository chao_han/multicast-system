################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/ForwardingTable.cpp \
../src/FwdTableData.cpp \
../src/HelloPktTimer.cpp \
../src/LinkLayer.cpp \
../src/LinkStateTable.cpp \
../src/LspPktTimer.cpp \
../src/Multicast.cpp \
../src/MulticastTableData.cpp \
../src/Node.cpp \
../src/PacketFormat.cpp \
../src/PortConfig.cpp \
../src/PortPair.cpp \
../src/Router.cpp \
../src/RouterGraph.cpp \
../src/Util.cpp \
../src/common.cpp \
../src/main.cpp 

OBJS += \
./src/ForwardingTable.o \
./src/FwdTableData.o \
./src/HelloPktTimer.o \
./src/LinkLayer.o \
./src/LinkStateTable.o \
./src/LspPktTimer.o \
./src/Multicast.o \
./src/MulticastTableData.o \
./src/Node.o \
./src/PacketFormat.o \
./src/PortConfig.o \
./src/PortPair.o \
./src/Router.o \
./src/RouterGraph.o \
./src/Util.o \
./src/common.o \
./src/main.o 

CPP_DEPS += \
./src/ForwardingTable.d \
./src/FwdTableData.d \
./src/HelloPktTimer.d \
./src/LinkLayer.d \
./src/LinkStateTable.d \
./src/LspPktTimer.d \
./src/Multicast.d \
./src/MulticastTableData.d \
./src/Node.d \
./src/PacketFormat.d \
./src/PortConfig.d \
./src/PortPair.d \
./src/Router.d \
./src/RouterGraph.d \
./src/Util.d \
./src/common.d \
./src/main.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


