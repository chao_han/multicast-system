/*
 * LinkLayer.cpp
 *
 *  Created on: May 2, 2013
 *      Author: chaohan
 */
#include"LinkLayer.h"


//constructor
LinkLayer::LinkLayer(){
	//myPacket = new Packet();
	seqNum = 0;
	//streamBuf = new char[linkLayerBufSize];
	//buffer = new std::string(linkLayerBufSize);
}
//destructor
LinkLayer::~LinkLayer(){

};

char LinkLayer::getSeqNum(){
	return seqNum;
}

void LinkLayer::setSeqNum(char num){
	seqNum = num;
}

void LinkLayer::switchSeqNum(){
	if(seqNum == 0){
		seqNum = 1;
		std::cout<<"0 -> 1"<<std::endl;
	}
	else{
		seqNum = 0;
		std::cout<<"1 -> 0"<<std::endl;
	}
}

Packet* LinkLayer::encapsulate(Packet *pkt){

	char *linkLayerBuf = new char[1499];

	int size = pkt->makePacket(linkLayerBuf);
	Packet *linkLayerPkt= new Packet();
	linkLayerPkt->setPayloadSize(linkLayerMaxPayloadSize);
	PacketHdr *hdr = linkLayerPkt->accessHeader();
	hdr->setOctet(seqNum, 0);
	linkLayerPkt->fillPayload(size, linkLayerBuf);

	return linkLayerPkt;
}

Packet* LinkLayer::extract(Packet *linkLayerPkt){
	Packet *pkt = new Packet();
	char* tempBuf = linkLayerPkt->getPayload();
	pkt->extractHeader(tempBuf);
	pkt->fillPayload(linkLayerPkt->getPayloadSize()-pkt->getHeaderSize(),
			tempBuf + pkt->getHeaderSize()+1 );
	return pkt;
}
