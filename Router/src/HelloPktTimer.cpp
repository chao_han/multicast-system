/*
 * HelloPktTimer.cpp
 *
 *  Created on: May 7, 2013
 *      Author: chaohan
 */

#include"HelloPktTimer.h"

HelloPktTimer::HelloPktTimer(Node *node){
	this->node = node;
	tdelay_.tv_nsec = 0;
	tdelay_.tv_sec =  0;
}

HelloPktTimer::~HelloPktTimer(){
	delete [] node;
}

void HelloPktTimer::startTimer(double delay){
	tdelay_.tv_nsec = (long int)((delay - (int)delay)*1e9);
	tdelay_.tv_sec =  (int)delay;
	int error = 0;
		error = pthread_create(&tid, NULL, &timerProc, this );
	if (error)
		throw "Timer thread creation failed...";
}

void HelloPktTimer::stopTimer(){
	  pthread_cancel(tid);
}


void *HelloPktTimer::timerProc(void *arg){
	HelloPktTimer *timer = (HelloPktTimer *)arg;
	nanosleep(&(timer->tdelay_), NULL);
	timer->node->helloPktTimerHandler();
	return NULL;
}

