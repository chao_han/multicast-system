/*
 * Router.cpp
 *
 *  Created on: May 2, 2013
 *      Author: chaohan
 */

#include"Router.h"


LinkStateTable *Router::linkStateTable = new LinkStateTable();
RouterGraph *Router::routerGraph = new RouterGraph();
pthread_mutex_t  Router::routerMutex = PTHREAD_MUTEX_INITIALIZER;
unsigned char Router::lspSeqNum = 0;

Router::Router():Node(){
	//lspSeqNum = 0;
	ports = new std::vector<PortPair*>();
	lspTimer = new LspPktTimer(this);
}

void Router::readConfigFile(){
	std::ifstream inFile;
	openFile(inFile, *configFilename);
	std::vector<int> *configinfo;
	PortConfig *config;

	while(inFile.peek() != EOF){
		configinfo = new std::vector<int>(5);
		inFile>>configinfo->at(0)>>configinfo->at(1)>>
				configinfo->at(2)>>configinfo->at(3)>>configinfo->at(4);
		if(configinfo->at(0) == nodeAddr){
			config = new PortConfig(configinfo->at(0),
					configinfo->at(1),configinfo->at(2),
					configinfo->at(3));
			ports->push_back(new PortPair(config));
			portCnt++;
		}
	}
	for(int i = 0; i != portCnt; i++){
		ports->at(i)->printConfigInfo();
	}
}

void Router::helloPktTimerHandler(){
	PacketFormat *pktfmt = new PacketHello(nodeAddr);
	int i = 0;
	for(std::vector<PortPair*>::iterator it = ports->begin();
			it != ports->end(); it++){

		//check flag before sending hello packet
		if((*it)->isAcked() == 1){
			std::cout<<"hello packet was sent from port "<<i<<std::endl;
			(*it)->send(pktfmt->getPacket());
		} else {
			pushBuf(pktfmt);
			std::cout<<"a hello pkt is put into buffer"<<std::endl;
		}
		i++;
	}
	helloTimer->stopTimer();
	helloTimer->startTimer(helloPktPeriod);    //0 hello Packet;
}

void Router::lspPktTimerHandler(){
	std::cout<<"Router LSP Packet Timer Function"<<std::endl;

	PacketFormat *pktfmt = new PacketLinkState(lspSeqNum,
			nodeAddr,getNeighborAddrs());
	checkLinkStateTable(nodeAddr, lspSeqNum);
	lspSeqNumIncrement();
	std::cout<<lspSeqNum<<std::endl;
	pktfmt->print();
	int i = 0;
	for(std::vector<PortPair*>::iterator it = ports->begin();
			it != ports->end(); it++){
		//check flag before sending lsp packet
		std::cout<<"lsp packet was sent from port "<<i<<std::endl;
		i++;
		if((*it)->isAcked()==1){
			(*it)->send(pktfmt->getPacket());
		} else {
			pushBuf(pktfmt);
		}
	}
	lspTimer->stopTimer();
	lspTimer->startTimer(lspPktPeriod);    //1 lsp Packet;
}

void Router::initialize(){
	routerGraph->setSize(10);
	readConfigFile();
	//prevLinkLayerSeqNums->resize(ports->size(),0);
	for(std::vector<PortPair*>::iterator iter = ports->begin();
			iter != ports->end(); iter++){
		(*iter)->initialize();
	}
}

void Router::receive(){
	// every port runs separately.
	pthread_t threads[portCnt];
	std::vector<PortThreadData*>  *td = new std::vector<PortThreadData*>(portCnt);
	std::cout<<portCnt<<std::endl;
	int rc;
	int i;
	for( i=0; i < portCnt; i++ ){
	      cout << "main() : creating thread, " << i << endl;
	      td->at(i) = new PortThreadData(i, ports->at(i));
	      rc = pthread_create(&threads[i], NULL,
	                          createPortThread, (void *)(td->at(i)));
	      if (rc){
	         cout << "Error:unable to create thread," << rc << endl;
	         exit(-1);
	      }
	}
	//pthread_exit(NULL);   //waiting for all above threads
}

void Router::process(){
	PacketFormat *pktFmt;
	std::cout<<"router's processor is running"<<std::endl;
	while(1){
		//if buffer in router is not empty
		if(!routerBuffer->empty()){
			std::cout<<"Buffer is not empty"<<std::endl;
			pktFmt = popBuf();
		} else {
			continue;
		}
		switch(pktFmt->getPktType()){
		case 0:
			//send it to all neighbors.

			sendHelloPkt(pktFmt);
			break;
		case 1:
			//sleep(1);
			sendHelloAck(pktFmt);
			break;
		case 2:

			sendLspPkt(pktFmt);
			break;
		case 3:
			std::cout<<"send data pkt"<<std::endl;
			sendDataPkt(pktFmt);
			break;
		default:
			break;
		}

	}
}

void Router::sendHelloPkt(PacketFormat *pktfmt){
	vector<int> *sentFlag = new vector<int>(portCnt,0);
	int cnt = 0;
	while(1){
		int i = 0;
		for(std::vector<PortPair*>::iterator iter = ports->begin();
				iter != ports->end(); iter++){
			if( ((*iter)->isAcked () == 1) && (sentFlag->at(i) == 0)){ //allow to send other packet
				std::cout<<"send Hello Packet from buffer"<<std::endl;
				(*iter)->send(pktfmt->getPacket());
				sentFlag->at(i) = 1;
				cnt++;
			}
			else if((*iter)->isAcked()){
				std::cout<<"port is busy"<<std::endl;
			}
			i++;
		}

		//if there is some port still not available
		if( cnt != i){
			continue;
		} else {
			cnt = 0;
			return;
		}
	}
}

void Router::sendHelloAck(PacketFormat *pktfmt){

	while(1){
		//int cnt = 0;
		for(std::vector<PortPair*>::iterator iter = ports->begin();
				iter != ports->end(); iter++){
			//if this port just send a hello packet, and hello ack is not sent yet;
			if( ((*iter)->isAcked() == 1) && ((*iter)->isHelloPktAcked() == 0)){
				std::cout<<"send Hello ACK from buffer"<<std::endl;
				(*iter)->send(pktfmt->getPacket());
				(*iter)->setHelloPktAcked(1);
			}
		}

		int cnt = 0;
		for(std::vector<PortPair*>::iterator iter = ports->begin();
				iter != ports->end(); iter++){
			//if still some packet is still not acked, break
			if((*iter)->isHelloPktAcked() == 0){
				cnt++;
			}
		}
		if(cnt == 0) {   //all hello ack are sent;
			break;
		}
	}
}

void Router::sendDataPkt(PacketFormat *pktfmt){
	int numOfAddrs = pktfmt->getPacket()->accessHeader()->getOctet(3);
	pktfmt->print();
	vector<vector<pair<unsigned char,unsigned char> > >  g=routerGraph->getGraph();

	if(numOfAddrs == 1){			//Unicast
		unsigned char dest = pktfmt->getPacket()->accessHeader()->getOctet(4);
		std::cout<<"dest is "<<int(dest)<<std::endl;
		vector<unsigned char> path ;//= new vector<unsigned char>();
		//routerGraph->printGraph();
		RouterGraph::dijkstra(g, nodeAddr, dest, path );
		routerGraph->setPath(path);
		std::cout<<"next hop is "<<int(routerGraph->getNextHop())<<std::endl;
		int sentCnt = 0;
		while(1){
			for(std::vector<PortPair*>::iterator it = ports->begin(); it != ports->end();
					it++){
				if((*it)->getPortConfig()->getNextHopAddr() ==
						routerGraph->getNextHop() && ((*it)->isAcked() == 1)){
					std::cout<<"send to destination"<<std::endl;
					(*it)->send(pktfmt->getPacket());
					sentCnt++;
					break;
				}
			}
			if(sentCnt == 1) {
				sentCnt = 0;
				break;
			}
		}
	} else {						 //Multicast
		Multicast *multicast = new Multicast();
		std::vector<unsigned char> *nextHop = new vector<unsigned char>();
		vector<unsigned char> *dest = new vector<unsigned char>();
		for(int i = 0; i != numOfAddrs; i++){
			dest->push_back(pktfmt->getPacket()->accessHeader()->getOctet(4+i));
		}
		nextHop = multicast->getNextHops(nodeAddr, dest, g);
		if(numOfAddrs == 2){
			if(nextHop->at(0) != nextHop->at(1)){
				//set destination addr size to 1;
				pktfmt->getPacket()->accessHeader()->setOctet(1,3);
				//send at nextHop->at(0)
				int sentCnt = 0;
				while(1){
					for(std::vector<PortPair*>::iterator it = ports->begin();
							it != ports->end();it++){
						if((*it)->getPortConfig()->getNextHopAddr() ==
								nextHop->at(0) && ((*it)->isAcked() == 1)){
							(*it)->send(pktfmt->getPacket());
							sentCnt++;
							break;
						}
					}
					if(sentCnt == 1) {
						sentCnt =0;
						break;
					}
				}


				//replace the first dest addr with the second one
				unsigned char dest = pktfmt->getPacket()->accessHeader()->getOctet(5);
				pktfmt->getPacket()->accessHeader()->setOctet(dest,4);
				while(1){
					for(std::vector<PortPair*>::iterator it = ports->begin();
							it != ports->end();it++){
						if((*it)->getPortConfig()->getNextHopAddr() ==
								nextHop->at(1) && ((*it)->isAcked() == 1)){
							(*it)->send(pktfmt->getPacket());
							sentCnt++;
							break;
						}
					}
					if(sentCnt == 1) {
						sentCnt =0;
						break;
					}
				}
			} else {

			}
				//send at nextHop->at(1)
					//else send at nextHop->at(0)
		} else if(numOfAddrs == 3){

			std::cout<<"if Number of Addr = 3"<<std::endl;
			unsigned char dest,dest1,dest2;
			PacketFormat *pkt = pktfmt;
			if( (nextHop->at(0)==nextHop->at(1)) && (nextHop->at(1)==nextHop->at(2)) ){
						/*if{
							// send at nextHOp->a(0);
						}*/
				std::cout<<"receive a packet have three idential dest addr"<<std::endl;
				int sentCnt = 0;
				while(1){
					for(std::vector<PortPair*>::iterator it = ports->begin();
							it != ports->end();it++){
						if((*it)->getPortConfig()->getNextHopAddr() ==
								nextHop->at(0) && ((*it)->isAcked() == 1)){
							(*it)->send(pkt->getPacket());
							sentCnt++;
							break;
						}
					}
					if(sentCnt == 1) {
						sentCnt =0;
						break;
					}
				}
			} else if((nextHop->at(0)==nextHop->at(1)) &&
					(nextHop->at(1) != nextHop->at(2))){
				pkt->getPacket()->accessHeader()->setOctet(2,3);
				int sentCnt = 0;
				while(1){
					for(std::vector<PortPair*>::iterator it = ports->begin();
							it != ports->end();it++){
						if((*it)->getPortConfig()->getNextHopAddr() ==
								nextHop->at(0) && ((*it)->isAcked() == 1)){
							(*it)->send(pkt->getPacket());
							sentCnt++;
							break;
						}

					}
					if(sentCnt == 1) {
						sentCnt =0;
						break;
					}
				}
				//send at nextHop->at(0)
				pkt->getPacket()->accessHeader()->setOctet(1,3);
				dest = pktfmt->getPacket()->accessHeader()->getOctet(6);
				pkt->getPacket()->accessHeader()->setOctet(dest,4);
				while(1){
					for(std::vector<PortPair*>::iterator it = ports->begin();
							it != ports->end();it++){
						if((*it)->getPortConfig()->getNextHopAddr() ==
								nextHop->at(2) && ((*it)->isAcked() == 1)){
							(*it)->send(pkt->getPacket());
							sentCnt++;
							break;
						}

					}
					if(sentCnt == 1) {
						sentCnt =0;
						break;
					}
				}
			}
			else if ((nextHop->at(0) != nextHop->at(2)) &&
					(nextHop->at(1)==nextHop->at(2))){
				pkt->getPacket()->accessHeader()->setOctet(1,3);
				//send at nextHop->at(0)
				int sentCnt = 0;
				while(1){
					for(std::vector<PortPair*>::iterator it = ports->begin();
							it != ports->end();it++){
						if((*it)->getPortConfig()->getNextHopAddr() ==
								nextHop->at(0) && ((*it)->isAcked() == 1)){
							(*it)->send(pkt->getPacket());
							sentCnt++;
							break;
						}

					} if(sentCnt == 1) {
						sentCnt =0;
						break;
					}
				}

				pkt->getPacket()->accessHeader()->setOctet(2,3);
				dest1 = pktfmt->getPacket()->accessHeader()->getOctet(5);
				pkt->getPacket()->accessHeader()->setOctet(dest1,4);
				dest2 = pktfmt->getPacket()->accessHeader()->getOctet(6);
				pkt->getPacket()->accessHeader()->setOctet(dest2,5);
				while(1){
					for(std::vector<PortPair*>::iterator it = ports->begin();
							it != ports->end();it++){
						if((*it)->getPortConfig()->getNextHopAddr() ==
								nextHop->at(1) && ((*it)->isAcked() == 1)){
							(*it)->send(pkt->getPacket());
							sentCnt++;
							break;
						}

					}
					if(sentCnt == 1) {
						sentCnt =0;
						break;
					}
				}
			} else if (nextHop->at(0) != nextHop->at(1) &&
					nextHop->at(0)==nextHop->at(2)){
				pkt->getPacket()->accessHeader()->setOctet(1,3);
				dest = pktfmt->getPacket()->accessHeader()->getOctet(5);
				pkt->getPacket()->accessHeader()->setOctet(dest,4);
				int sentCnt = 0;
				while(1){
					for(std::vector<PortPair*>::iterator it = ports->begin();
						it != ports->end();it++){
						if((*it)->getPortConfig()->getNextHopAddr() ==
								nextHop->at(1) && ((*it)->isAcked() == 1)){
							(*it)->send(pkt->getPacket());
							sentCnt++;
							break;
						}

					}
					if(sentCnt == 1) {
						sentCnt =0;
						break;
					}
				}
				pkt->getPacket()->accessHeader()->setOctet(2,3);
				dest1 = pktfmt->getPacket()->accessHeader()->getOctet(4);
				pkt->getPacket()->accessHeader()->setOctet(dest1,4);
				dest2 = pktfmt->getPacket()->accessHeader()->getOctet(6);
				pkt->getPacket()->accessHeader()->setOctet(dest2,5);
				//send at nextHop->at(0)
				while(1){
					for(std::vector<PortPair*>::iterator it = ports->begin();
							it != ports->end();it++){
						if((*it)->getPortConfig()->getNextHopAddr() ==
								nextHop->at(0) && ((*it)->isAcked() == 1)){
							(*it)->send(pkt->getPacket());
							sentCnt++;
							break;
						}

					}
					if(sentCnt == 1) {
						sentCnt =0;
						break;
					}
				}
			} else{
				pkt->getPacket()->accessHeader()->setOctet(1,3);
				//send at nextHop->at(0)
				int sentCnt = 0;
				while(1){
					for(std::vector<PortPair*>::iterator it = ports->begin();
							it != ports->end();it++){
						if((*it)->getPortConfig()->getNextHopAddr() ==
								nextHop->at(0) && ((*it)->isAcked() == 1)){
							(*it)->send(pkt->getPacket());
							sentCnt++;
							break;
						}

					}
					if(sentCnt == 1) {
						sentCnt =0;
						break;
					}
				}

				dest = pktfmt->getPacket()->accessHeader()->getOctet(5);
				pkt->getPacket()->accessHeader()->setOctet(dest,4);

				while(1){
					for(std::vector<PortPair*>::iterator it = ports->begin();
							it != ports->end();it++){
						if((*it)->getPortConfig()->getNextHopAddr() ==
								nextHop->at(1) && ((*it)->isAcked() == 1)){
							(*it)->send(pkt->getPacket());
							sentCnt++;
							break;
						}

					}
					if(sentCnt == 1) {
						sentCnt =0;
						break;
					}
				}
				//send at nextHop->at(1)
				dest = pktfmt->getPacket()->accessHeader()->getOctet(6);
				pkt->getPacket()->accessHeader()->setOctet(dest,4);
				while(1){
					for(std::vector<PortPair*>::iterator it = ports->begin();
							it != ports->end();it++){
						if((*it)->getPortConfig()->getNextHopAddr() ==
								nextHop->at(2) && ((*it)->isAcked() == 1)){
							(*it)->send(pkt->getPacket());
							sentCnt++;
							break;
						}

					}
					if(sentCnt == 1) {
						sentCnt =0;
						break;
					}
				}
				//send at nextHop->at(2)
			}
		}
	}
}

void Router::sendLspPkt(PacketFormat *pktfmt){
	vector<int> *sentFlag = new vector<int>(portCnt,0);
	int cnt = 0;
	while(1){
		int i = 0;
		for(std::vector<PortPair*>::iterator iter = ports->begin();
				iter != ports->end(); iter++){
			if( ((*iter)->isAcked () == 1) && (sentFlag->at(i) == 0)){ //allow to send other packet
				std::cout<<"send LSP Packet from buffer"<<std::endl;
				(*iter)->send(pktfmt->getPacket());
				sentFlag->at(i) = 1;
				cnt++;
			}
			else if((*iter)->isAcked()){
				std::cout<<"port is busy"<<std::endl;
			}
			i++;
		}

		//if there is some port still not available
		if( cnt != i){
			//std::cout<<cnt<<" "<<i<<std::endl;
			//std::cout<<"waiting for some port"<<std::endl;
			continue;
		} else {
			cnt = 0;
			//break;
			return;
		}
	}
}

void *Router::createPortThread(void* threadData){
	PortThreadData *data;
	data = (PortThreadData *) threadData;
	std::cout<<"Thread ID: "<<data->threadId;
	data->portPair->receive();

	pthread_exit(NULL);
}


std::vector<unsigned char>* Router::getNeighborAddrs(){
	std::vector<unsigned char> *neighborAddrs = new std::vector<unsigned char>();
	for(std::vector<PortPair*>::iterator iter = ports->begin();
			iter != ports->end(); iter++){
		//add addrs of neighbors into a vector.
		neighborAddrs->push_back((*iter)->getPortConfig()->getNextHopAddr());
	}
	return neighborAddrs;
}

bool Router::checkLinkStateTable(unsigned char srcAddr, unsigned char linkStateSeqNum){
	return linkStateTable->checkTable(srcAddr, linkStateSeqNum);
}

void Router::printLinkStateTable(){
	linkStateTable->printTable();
}


void Router::updateRouterGraph(unsigned char src, vector<unsigned char> *dests){
	routerGraph->update(src, *dests);
}

void Router::printRouterGraph(){
	routerGraph->printGraph();
}

RouterGraph *Router::getRouterGraph(){
	return routerGraph;
}

void Router::lspSeqNumIncrement(){
	pthread_mutex_lock(&routerMutex);
	lspSeqNum++;
	pthread_mutex_unlock(&routerMutex);
}
