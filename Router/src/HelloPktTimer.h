/*
 * HelloPktTimer.h
 *
 *  Created on: May 7, 2013
 *      Author: chaohan
 */

#ifndef HELLOPKTTIMER_H_
#define HELLOPKTTIMER_H_

#include<pthread.h>
#include"Node.h"
class Node;

class HelloPktTimer{
	//friend class Node;
public:
	HelloPktTimer(Node *);
	void startTimer(double delay);
	void stopTimer();
	//create a separate thread;
	static void *timerProc(void *arg) ;
	~HelloPktTimer();
protected:
	Node *node;
	struct timespec tdelay_;
	pthread_t tid;
};


#endif /* HELLOPKTTIMER_H_ */
