/*
 * Node.cpp
 *
 *  Created on: May 1, 2013
 *      Author: chaohan
 */

#include"Node.h"


std::queue<PacketFormat*> *Node::routerBuffer = new std::queue<PacketFormat*>();
std::string *Node::configFilename = new std::string("//home//chaohan//config.ini");
unsigned char Node::nodeAddr = -1;


Node::Node():portCnt(0){
	helloTimer = new HelloPktTimer(this);
}

Node::Node(int cnt): portCnt(cnt){
	helloTimer = new HelloPktTimer(this);
}

Node::~Node(){
	delete [] helloTimer;

}

void Node::setNodeAddr(unsigned char addr){
	nodeAddr = addr;
}

void Node::setPortCnt(int cnt){
	portCnt = cnt;
}

unsigned char Node::getNodeAddr(){
	return nodeAddr;
}

int Node::getPortCnt(){
	return portCnt;
}


PacketFormat* Node::popBuf(){
	//pthread_mutex_lock(&myMutex);
	PacketFormat* temp = routerBuffer->front();
	routerBuffer->pop();
	//pthread_mutex_unlock(&myMutex);
	return temp;
}

void Node::pushBuf(PacketFormat* pkt){
	//pthread_mutex_lock(&myMutex);
	routerBuffer->push(pkt);
	//pthread_mutex_unlock(&myMutex);
}
