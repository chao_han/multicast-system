/*
 * Config.h
 *
 *  Created on: May 1, 2013
 *      Author: chaohan
 */

#ifndef CONFIG_H_
#define CONFIG_H_

#include<iostream>
#include<string>
#include<fstream>
#include"Util.h"

const int invalidAddr = -1;
const int invalidPortNum = -1;

class PortConfig {
public:
	PortConfig();
	PortConfig(int, int, int, int);
	PortConfig(int, int, int, int, int);

	//setter
	void setSendingPortNo(int);
	void setReceivingPortNo(int);
	void setNextHopPortNo(int);
	void setNextHopAddr(int);
	void setAddr(int);

	//getter
	int getSendingPortNo();
	int getReceivingPortNo();
	int getNextHopPortNo();
	int getNextHopAddr();
	int getAddr();

	void printPortConfig();

private:
	int addr;
	int sendingPortNo;
	int receivingPortNo;
	int nextHopPortNo;
	int nextHopAddr;

};

#endif /* CONFIG_H_ */
