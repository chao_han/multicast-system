/*
 * Router.h
 *
 *  Created on: May 2, 2013
 *      Author: chaohan
 */

#ifndef ROUTER_H_
#define ROUTER_H_

#include<vector>
#include<iostream>
#include<pthread.h>
#include"LinkLayer.h"
#include"Node.h"
#include"LinkStateTable.h"
#include"PortPair.h"
#include<utility>
#include"PortThreadData.h"
#include"RouterGraph.h"
#include"PacketFormat.h"
#include"Multicast.h"
#include"LspPktTimer.h"

class PortPair;
const int helloPktPeriod = 30;    //30s
const int lspPktPeriod   = 120;   //120s


class PacketFormat;
class LspPktTimer;

class Router: public Node{
public:
	Router();
	void send();
	void receive();
	void process();
	void initialize();
	void helloPktTimerHandler();
	void lspPktTimerHandler();

	static bool checkLinkStateTable(unsigned char, unsigned char);
	static void lspSeqNumIncrement();
	static void printLinkStateTable();
	static void updateRouterGraph(unsigned char src, vector<unsigned char> *dests);
	static void printRouterGraph();
	static RouterGraph* getRouterGraph();
	//static unsigned char getRouterAddr();


private:
	void sendHelloPkt(PacketFormat *);
	void sendHelloAck(PacketFormat *);
	void sendLspPkt(PacketFormat *);
	void sendDataPkt(PacketFormat *);

	static void *createPortThread(void *);
	void readConfigFile();
	std::vector<unsigned char> *getNeighborAddrs();

	std::vector<PortPair*>  *ports;
	LspPktTimer *lspTimer;

	static unsigned char lspSeqNum;

	static LinkStateTable *linkStateTable;

	static RouterGraph *routerGraph;
	static pthread_mutex_t  routerMutex;
};



#endif /* ROUTER_H_ */
