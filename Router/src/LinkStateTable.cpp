/*
 * LinkStateSeq.cpp
 *
 *  Created on: 2013-5-4
 *      Author: lee
 */

#include "LinkStateTable.h"



LinkStateTable::LinkStateTable() {
	// TODO Auto-generated constructor stub
	std::cout<<"linkStateTableConstructor"<<std::endl;
	linkStateTable = new std::map<unsigned char,unsigned char>();
}

/**
 * check table whether the node need to send the lsp received
 * 0 stop sending;
 * 1 keep sending;
 */
bool LinkStateTable::checkTable(unsigned char scrAddr, unsigned char linkStateSeqNum){
	std::map<unsigned char, unsigned char >::iterator it = linkStateTable->find(scrAddr);
	if( it == linkStateTable->end()){        //if the scrAddr does not exist in the table of state table,
		linkStateTable->insert(std::make_pair(scrAddr, linkStateSeqNum));
		return true;
	} else {
		if( it->second < linkStateSeqNum ){
			it->second = linkStateSeqNum;
			return true;
		} else{
			return false;
		}
	}
	return false;
}

void LinkStateTable::printTable(){
	for(std::map<unsigned char, unsigned char>::iterator mapIter = linkStateTable->begin();
			mapIter != linkStateTable->end(); mapIter++){
		std::cout<<(int)mapIter->first<<"  "<<(int)mapIter->second<<std::endl;
	}
}

LinkStateTable::~LinkStateTable() {
	// TODO Auto-generated destructor stub
	delete [] linkStateTable;
}

