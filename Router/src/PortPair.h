/*
 * Port.h
 *
 *  Created on: May 1, 2013
 *      Author: chaohan
 */

#ifndef PORTPAIR_H_
#define PORTPAIR_H_

#include"PortConfig.h"
#include"common.h"
#include"Router.h"
#include"newport.h"
#include"PacketFormat.h"
#include"LinkLayer.h"

const double pktLossProbability = 0.0;

class PortPair{
public:
	PortPair();
	PortPair(PortConfig *config);

	void setPortConfig(PortConfig *);
	PortConfig* getPortConfig();

	//whether a Hello Ack Pkt received after sending a Hello Pkt
	bool isHelloPktAcked(){
		return isHelloAcked;
	}
	void setHelloPktAcked(bool ackedOrNot){
		isHelloAcked = ackedOrNot;
	}

	//whether the port is allow to send other Pkt;
	bool isAcked(){

		return txPort->isACKed();

	}

	void initialize();
	void send(Packet *pkt);
	void sendLinkLayerAck(Packet *pkt);
	void sendHello();
	void sendHelloAck();

	void receive();

	char getLinkLayerSeq();
	void setLinkLayerSeq(char seq);
	void switchLinkLayerSeq();

	void printConfigInfo();

	~PortPair();
private:
	PortConfig *conf;
	mySendingPort *txPort;
	LossyReceivingPort *rxPort;
	LinkLayer *linkLayer;
	bool isHelloAcked;
	pthread_mutex_t portMutex;
};


#endif /* PORT_H_ */
