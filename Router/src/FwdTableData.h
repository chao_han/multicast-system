/*
 * ForwardPair.h
 *
 *  Created on: 2013-5-2
 *      Author: lee
 */



#ifndef FWDTABLEDATA_H_
#define FWDTABLEDATA_H_

#include <utility>
#include <vector>

class FwdTableData {
public:
	FwdTableData(unsigned char,int,int, unsigned char);
	int getCost();
	unsigned char getDestAddr();
	std::pair<unsigned char,unsigned char> getPortNumAndNextHop();
	void setPortNumAndNextHop(int port,unsigned char nextHop);

	virtual ~FwdTableData();
private:
	int port;
	unsigned char destAddr;
	unsigned char cost;	
	unsigned char nextHop;
	std::pair<int,unsigned char> portNumAndNextHop;
};

#endif /* FORWARDPAIR_H_ */
