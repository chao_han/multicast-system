/*
 * Multicast.h
 *
 *  Created on: May 5, 2013
 *      Author: loki
 */

#ifndef MULTICAST_H_
#define MULTICAST_H_
#include <vector>
#include "MulticastTableData.h"
#include "ForwardingTable.h"
#include<iostream>
using namespace std;

class Multicast {
public:
	Multicast();
	virtual ~Multicast();
	void buildMultiForwardingTable(unsigned char srcAddr, std::vector<unsigned char> *destAddr, vector<vector<pair<unsigned char,unsigned char> > > routerGraph);
		//build forwarding tables for the router and it's next hop
	void buildNextHopCandidates(unsigned char srcAddr,std::vector<unsigned char> *destAddr);
		//build list of possible next hop for every destination
	std::vector<unsigned char> *getMulticastNextHops();
		//calculate the best solution
	std::vector<unsigned char> *getNextHops(unsigned char srcAddr, std::vector<unsigned char> *destAddr,vector<vector<pair<unsigned char,unsigned char> > > routerGraph);
		// use getNextHops to get solution(all in one)
private:
	std::vector<ForwardingTable *> *allForwardingTable;
	std::vector<std::vector<unsigned int> > *nextHopCandidates;
	std::vector<MulticastTableData*> *multicastTable;

};

#endif /* MULTICAST_H_ */
