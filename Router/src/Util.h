/*
 * Util.h
 *
 *  Created on: May 1, 2013
 *      Author: chaohan
 */

#ifndef UTIL_H_
#define UTIL_H_

#include<iostream>
#include<fstream>
#include<string>
#include<utility>

extern std::ifstream& openFile(std::ifstream &in, const std::string &file);

class Comparator {
public:
	Comparator();
	int operator()(const std::pair<int,float>&, const std::pair<int,float>&);
	virtual ~Comparator();
private:
};


#endif /* UTIL_H_ */
