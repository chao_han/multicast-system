/*
 * LinkStateSeq.h
 *
 *  Created on: 2013-5-4
 *      Author: lee
 */

#ifndef LINKSTATETABLE_H_
#define LINKSTATETABLE_H_
#include<iostream>
#include<vector>
#include<map>
#include<utility>
using std::vector;
using std::pair;

class LinkStateTable {
public:
	LinkStateTable();
	//void updateTable();
	bool checkTable(unsigned char,unsigned char);

	void printTable();
	~LinkStateTable();


private:
	std::map<unsigned char, unsigned char>  *linkStateTable;
};


#endif /* LINKSTATESEQ_H_ */
