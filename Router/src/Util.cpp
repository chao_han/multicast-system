/*
 * Util.cpp
 *
 *  Created on: May 1, 2013
 *      Author: chaohan
 */

#include"Util.h"

std::ifstream& openFile(std::ifstream &in, const std::string &file)
{
    in.close();     	// close in case it was already open
    in.clear();    		 // clear any existing errors
    // if the open fails, the stream will be in an invalid state
    in.open(file.c_str());  // open the file we were given
    if(in.fail()) {
    	std::cout<<"cannot open file"<<std::endl;
    }
    return in;      // condition state is good if open succeeded
}


Comparator::Comparator(){

}

int Comparator::operator()(const std::pair<int,float> &p1, const std::pair<int,float> &p2){
		return p1.second>p2.second;
}

Comparator::~Comparator() {
	// TODO Auto-generated destructor stub
}
