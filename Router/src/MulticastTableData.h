/*
 * MulticastTableData.h

 *
 *  Created on: May 5, 2013
 *      Author: loki
 */

#ifndef MULTICASTTABLEDATA_H_
#define MULTICASTTABLEDATA_H_
#include <vector>

class MulticastTableData {
public:
	MulticastTableData();
	MulticastTableData(std::vector<unsigned char> *nextHopForEachDest, std::vector<int> *nCost);
		// vector of nexthop for each dest and the corresponding cost
	virtual ~MulticastTableData();
	void print();
	double getCost();
	std::vector<unsigned char> *getnextHop();
private:
	std::vector<unsigned char> *nextHop;
	double cost;
};

#endif /* MULTICASTTABLEDATA_H_ */
