#include "PacketFormat.h"

/*****************Class PacketFormat****************************/
PacketFormat::PacketFormat(){
    my_packet = new Packet();
}
PacketFormat::PacketFormat(Packet *pkt):my_packet(pkt){
}

PacketFormat::~PacketFormat(){
	delete [] my_packet;
};

Packet* PacketFormat::getPacket() {
    return my_packet;
}

void PacketFormat::setPacket(Packet *pkt){
	my_packet = pkt;
}

unsigned char PacketFormat::getPktType(){
	return my_packet->accessHeader()->getOctet(0);
}

/*char PacketFormat::getSrcAddr(){
	return my_packet->accessHeader()->getOctet(1);
}*/
/****************************Hello Packet**********************************/
PacketHello::PacketHello(unsigned char SrcAddr) {
    my_packet->setPayloadSize(0);
    PacketHdr *hdr = my_packet->accessHeader();
    hdr->setOctet(0,0);
    hdr->setOctet(SrcAddr,1);
}

unsigned char PacketHello::getSrcAddr(){
	return my_packet->accessHeader()->getOctet(1);
}

PacketHello::PacketHello(Packet *pkt):PacketFormat(pkt){

}
bool PacketHello::processPacket(){
	return 1;
}
void PacketHello::print(){
	std::cout<<(int)my_packet->accessHeader()->getOctet(0)<<" "
				<<(int)my_packet->accessHeader()->getOctet(1)<<std::endl;
}
/*****************************Hello ACK************************************/
PacketHelloAck::PacketHelloAck(unsigned char SrcAddr) {
    my_packet->setPayloadSize(0);
    PacketHdr *hdr = my_packet->accessHeader();
    hdr->setOctet(1,0);
    hdr->setOctet(SrcAddr,1);
}
PacketHelloAck::PacketHelloAck(Packet *pkt):PacketFormat(pkt){

}

unsigned char PacketHelloAck::getSrcAddr(){
	return my_packet->accessHeader()->getOctet(1);
}

bool PacketHelloAck::processPacket(){
	return 1;
}
void PacketHelloAck::print(){
	std::cout<<(int)my_packet->accessHeader()->getOctet(0)<<" "
				<<(int)my_packet->accessHeader()->getOctet(1)<<std::endl;
}
/*****************************Link State Packet*****************************/
PacketLinkState::PacketLinkState(unsigned char SeqNum, unsigned char SrcAddr,
                              std::vector<unsigned char> *neighborAddrs) {
    my_packet->setPayloadSize(0);
    PacketHdr *hdr = my_packet->accessHeader();
    hdr->setOctet(2,0);
    hdr->setOctet(SeqNum,1);
    hdr->setOctet(SrcAddr,2);
    char lAddr = neighborAddrs->size();
    hdr->setOctet(lAddr,3);
    int addrOffset = 0;
    for(std::vector<unsigned char>::iterator iter=neighborAddrs->begin();
     	iter != neighborAddrs->end();iter++){
       	hdr->setOctet(*iter,4 + addrOffset);
       	addrOffset++;
    }
}

PacketLinkState::PacketLinkState(Packet *pkt):PacketFormat(pkt){

}

unsigned char PacketLinkState::getSrcAddr(){
	return my_packet->accessHeader()->getOctet(2);
}
/**
 * return drop packet or not
 * 1 drop packet;
 * 0 not drop packet;
 */
bool PacketLinkState::processPacket(){
 	print();
	char srcAddr = getSrcAddr();
	char seqNum =  getSeqNum();
	if(Router::checkLinkStateTable(srcAddr, seqNum) == true){
		int neighborCnt = getNeighborCnt();
		std::vector<unsigned char> *neighborAddrs =
				new std::vector<unsigned char>();
		for( int i = 0; i != neighborCnt ; i++){
			neighborAddrs->push_back(getNeighborAddr(i));
		}
		Router::updateRouterGraph(srcAddr, neighborAddrs);
		Router::printLinkStateTable();
		Router::printRouterGraph();
		return 1;
	} else {
		return 0;              //drop packet;
	}
}

char PacketLinkState::getSeqNum(){
	return my_packet ->accessHeader()->getOctet(1);
}

char PacketLinkState::getNeighborCnt(){
	return my_packet->accessHeader()->getOctet(3);
}

//Neighbor Addr is from 1, Not 0;
char PacketLinkState::getNeighborAddr(int index){
	return my_packet->accessHeader()->getOctet(index+4);
}

void PacketLinkState::print(){
	std::cout<<(int)my_packet->accessHeader()->getOctet(0)<<" "
			<<(int)my_packet->accessHeader()->getOctet(1)<<" "
			<<(int)my_packet->accessHeader()->getOctet(2)<<" "
			<<(int)my_packet->accessHeader()->getOctet(3)<<std::endl;
	int nPort= (int)(my_packet->accessHeader()->getOctet(3));
	for(int i=0;i<nPort;i++)
	std::cout<<(int)my_packet->accessHeader()->getOctet(4+i)<<" ";
	std::cout<<std::endl;
};

/*************************Data Packet****************************************/
PacketData::PacketData(unsigned char TTL, unsigned char srcAddr,
		std::vector<unsigned char> *destAddrSet, const std::string &msg) {
    my_packet->setPayloadSize(1490);
    PacketHdr *hdr = my_packet->accessHeader();
    hdr->setOctet(3,0);
    hdr->setOctet(TTL,1);
    hdr->setOctet(srcAddr,2);
    char nAddr = destAddrSet->size();
    hdr->setOctet(nAddr,3);
    int addrOffset = 0;
    for(std::vector<unsigned char>::iterator iter=destAddrSet->begin();
    		iter != destAddrSet->end();iter++){
    	hdr->setOctet(*iter,4 + addrOffset);
    	addrOffset++;
    }
    int msgSize = msg.size()+1;
    std::cout<<msgSize<<std::endl;
	hdr->setShortIntegerInfo(msgSize,7);
	my_packet->fillPayload(msgSize, msg.c_str());
}

PacketData::PacketData(Packet *pkt):PacketFormat(pkt){

}

unsigned char PacketData::getSrcAddr(){
	return my_packet->accessHeader()->getOctet(2);
}

char PacketData::getTTL(){
	return my_packet ->accessHeader()->getOctet(1);
}

char PacketData::getNumOfAddrs(){
	return my_packet ->accessHeader()->getOctet(3);
}

char PacketData::getDestAddr(int index){
	return my_packet->accessHeader()->getOctet(index+4);
}

int PacketData::getDataSize(){
	return my_packet ->accessHeader()->getIntegerInfo(7);
}

void PacketData::setTTL(char ttl){
	PacketHdr *hdr = my_packet->accessHeader();
	hdr->setOctet(ttl,1);
}

void PacketData::setDesAddrs(std::vector<unsigned char> *destAddrSet){
	char nAddr = destAddrSet->size();
	PacketHdr *hdr = my_packet->accessHeader();
	hdr->setOctet(nAddr,3);
	int addrOffset = 0;
	for(std::vector<unsigned char>::iterator iter=destAddrSet->begin();
	    iter != destAddrSet->end();iter++){
	    hdr->setOctet(*iter,4 + addrOffset);
	    addrOffset++;
	}
}

bool PacketData::processPacket(){
	unsigned char numOfDest = getNumOfAddrs();
	unsigned char ttl = getTTL()-1;
	if( (numOfDest == 1) && (getDestAddr(0) == Node::getNodeAddr())){ 		//uniCast
		std::cout<<"this is destination"<<std::endl;
		print();
		return false;
	} else if ( ttl != 0) {
		print();
		std::cout<<"receive data packet, forward to other ports"<<std::endl;
		setTTL(ttl);
		return true;		// sent this data packet to the nextHop
	} else {
		std::cout<<"error?"<<std::endl;
		return false;
	}
}

void PacketData::print(){
	std::cout<<"-----------------------------------------------"<<std::endl;
	std::cout<<(int)my_packet->accessHeader()->getOctet(0)<<" "
			<<(int)my_packet->accessHeader()->getOctet(1)<<" "
			<<(int)my_packet->accessHeader()->getOctet(2)<<" "
			<<(int)my_packet->accessHeader()->getOctet(3)<<std::endl;
	std::cout<<(int)my_packet->accessHeader()->getOctet(4)<<" "
			<<(int)my_packet->accessHeader()->getOctet(5)<<" "
			<<(int)my_packet->accessHeader()->getOctet(6)<<" "
			<<(int)my_packet->accessHeader()->getShortIntegerInfo(7)<<std::endl;
	std::string data(my_packet->getPayload());
	std::cout<<data<<std::endl;
	std::cout<<"------------------------------------------------"<<std::endl;
}
