/*
 * Multicast.cpp
 *
 *  Created on: May 5, 2013
 *      Author: loki
 */

#include "Multicast.h"


Multicast::Multicast() {
	// TODO Auto-generated constructor stub
	allForwardingTable = new std::vector<ForwardingTable *> ();
	multicastTable = new std::vector<MulticastTableData*> ();
	nextHopCandidates = new std::vector<std::vector<unsigned int> >();
	nextHopCandidates->resize(10);
}

Multicast::~Multicast() {
	// TODO Auto-generated destructor stub
}

void Multicast::buildMultiForwardingTable(unsigned char srcAddr, std::vector	<unsigned char> *destAddr,vector<vector<pair<unsigned char,unsigned char> > > routerGraph){
	ForwardingTable *tableOrigin = new ForwardingTable(srcAddr, destAddr,routerGraph);
	allForwardingTable->push_back(tableOrigin);
	unsigned int i;
	for(i=0;i<destAddr->size();i++){
		allForwardingTable->push_back(new ForwardingTable(tableOrigin->getNextHop().at(i), destAddr,routerGraph));
	}
}

void Multicast::buildNextHopCandidates(unsigned char srcAddr,std::vector<unsigned char> *destAddr){
	unsigned int i,j;
	for(i=0;i<destAddr->size();i++){				 // for each destination
		int min = allForwardingTable->at(0)->getCost().at(i);  			 // set cost as origin cost from R1
			for(j=1;j<allForwardingTable->size();j++){  		 // for each fwdtable of the neighbor
				if(allForwardingTable->at(j)->getCost().at(i)<=min)  	 // if has smaller distance from next-hop
					if(allForwardingTable->at(j)->getNextHop().at(i)!=srcAddr){ //check nexthop to avoid loop (not R1)
						nextHopCandidates->at(i).push_back(j-1); 	 //save the index of nexthop candidates in
					
						//cout<<" "<<j-1;
					//the ith row in the nextHopCandidates which stands for i+1 th destination
					//the index of the nexthop candidates is the same as the nexthop in fwd table of R1
					}
			}
			//cout<<endl;
	}
}

std::vector<unsigned char> *Multicast::getMulticastNextHops(){
	unsigned int i,j,k;
	double min=100;
	int nextHopIndex1,nextHopIndex2,nextHopIndex3;
	std::vector<unsigned char> *finalNextHop;
	std::vector<unsigned char> *nextHopForEachDest;
	std::vector<int> *nCost;
	if(allForwardingTable->size()==3){
		for(i=0;i<nextHopCandidates->at(0).size();i++){
			for(j=0;j<nextHopCandidates->at(1).size();j++){
				nextHopForEachDest= new std::vector<unsigned char>();
				nCost = new std::vector<int>;
				 nextHopIndex1=nextHopCandidates->at(0).at(i);
				nextHopIndex2=nextHopCandidates->at(1).at(j);
				nextHopForEachDest->push_back(allForwardingTable->at(0)->getNextHop()[nextHopIndex1]);
				nextHopForEachDest->push_back(allForwardingTable->at(0)->getNextHop()[nextHopIndex2]);
					//convert the index stored in nextHopCandidates to the actual address
				nCost->push_back(allForwardingTable->at(nextHopIndex1+1)->getCost()[0]);
				nCost->push_back(allForwardingTable->at(nextHopIndex2+1)->getCost()[1]);
					//get the corresponding cost for each nexthop
				MulticastTableData *mtd = new MulticastTableData(nextHopForEachDest,nCost);
				if(mtd->getCost()<min){ //compare the multicast cost
					min = mtd->getCost();
					finalNextHop = nextHopForEachDest;
				}
			}
		}
	}
	else if(allForwardingTable->size()==4){ 
		for(i=0;i<nextHopCandidates->at(0).size();i++){
			for(j=0;j<nextHopCandidates->at(1).size();j++){
				for(k=0;k<nextHopCandidates->at(2).size();k++){
					nextHopForEachDest= new std::vector<unsigned char>();
					nCost = new std::vector<int>;
					nextHopIndex1=nextHopCandidates->at(0).at(i);
					nextHopIndex2=nextHopCandidates->at(1).at(j);
					nextHopIndex3=nextHopCandidates->at(2).at(k);
					nextHopForEachDest->push_back(allForwardingTable->at(0)->getNextHop()[nextHopIndex1]);
					nextHopForEachDest->push_back(allForwardingTable->at(0)->getNextHop()[nextHopIndex2]);
					nextHopForEachDest->push_back(allForwardingTable->at(0)->getNextHop()[nextHopIndex3]);
					nCost->push_back(allForwardingTable->at(nextHopIndex1+1)->getCost()[0]);
					nCost->push_back(allForwardingTable->at(nextHopIndex2+1)->getCost()[1]);
					nCost->push_back(allForwardingTable->at(nextHopIndex3+1)->getCost()[2]);
					/*for(int m=0;m<nextHopForEachDest->size();m++)cout<<" "<<int(nextHopForEachDest->at(m));
					for(int m=0;m<nCost->size();m++)cout<<" "<<int(nCost->at(m));*/
					MulticastTableData *mtd = new MulticastTableData(nextHopForEachDest,nCost);
					//cout<<mtd->getCost()<<endl;
					//cout<<min<<endl;
					if(mtd->getCost()<min){
						//cout<<"update!!!"<<endl;
						min = mtd->getCost();
						finalNextHop = nextHopForEachDest;
					}
				}
			}
		}
	}
	for(std::vector<unsigned char>::iterator it = finalNextHop->begin();
			it!= finalNextHop->end(); it++){
		std::cout<<(int)*it<<" "<<std::endl;
	}
	std::cout<<std::endl;
	return finalNextHop;
}

std::vector<unsigned char> *Multicast::getNextHops(unsigned char srcAddr, std::vector<unsigned char> *destAddr,vector<vector<pair<unsigned char,unsigned char> > > routerGraph){
	buildMultiForwardingTable(srcAddr,destAddr,routerGraph);
	buildNextHopCandidates(srcAddr,destAddr);
	return getMulticastNextHops();
}


