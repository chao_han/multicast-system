/*
 * main.cpp
 *
 *  Created on: May 1, 2013
 *      Author: chaohan
 */
//#include"PortConfig.h"

#include"Router.h"
#include<iostream>
#include<pthread.h>

int main(int argc, const char * argv[]){
	std::cout<<"hello world"<<std::endl;
	Router * router = new Router();
	router->setNodeAddr(atoi(argv[1]));
	router->initialize();
	HelloPktTimer *timer1 = new HelloPktTimer(router);
	LspPktTimer *timer2 = new LspPktTimer(router);
	timer1->startTimer(15.0);
	timer2->startTimer(35.0);

	router->receive();
	router->process();

	//std::cout<<"program end"<<std::endl;
	pthread_exit(NULL);

	return 0;
}


